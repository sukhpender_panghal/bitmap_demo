package com.example.bitmap_demo

import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.ceil


class MainActivity : AppCompatActivity() {

    var image_uri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_capture.setOnClickListener {

            if (checkSelfPermission(Manifest.permission.CAMERA) ==
                PackageManager.PERMISSION_DENIED ||
                checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_DENIED){

                val permission = arrayOf(Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)

                requestPermissions(permission,100)
            }
            else{
                openCamera()
            }
        }
    }
    private fun openCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")
        image_uri = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri)
        startActivityForResult(cameraIntent, 100)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {

            bitmap(image_uri.toString(), 150,150)
            //image_view.setImageBitmap(bMapScaled)
            //image_view.setImageURI(image_uri)
            Toast.makeText(this, "captured", Toast.LENGTH_SHORT).show()
        }
    }

    fun bitmap(file: String?, width: Int, height: Int): Bitmap? {

        val bmp = BitmapFactory.Options()
        bmp.inJustDecodeBounds = true

        val bitmap : Bitmap?
        val height = ceil(bmp.outHeight / height.toDouble()).toInt()
        val width = ceil(bmp.outWidth / width.toDouble()).toInt()

        if (height > 1 || width > 1) {
            if (height > width) {
                bmp.inSampleSize = height
            } else {
                bmp.inSampleSize = width
            }
        }
        bmp.inJustDecodeBounds = false
        bitmap = BitmapFactory.decodeFile(file, bmp)
        image_view.setImageResource(bitmap.toString().toInt())
        return bitmap
    }
}